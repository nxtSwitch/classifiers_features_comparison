# stanza.download('en')  
# nltk.download('punkt')

# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html
# https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
# https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)

import time
import json
import nltk
import stanza
import numpy as np
import emoji
import re
import os, sys
import threading
# stanza.download('ru')  

from pymystem3 import Mystem
from nltk.stem import ISRIStemmer 
from nltk.tokenize import word_tokenize

lock = threading.Lock()
lock2 = threading.Lock()

file = open("stopwords.txt", "r")
func_words = file.read().split('\n')
file.close()

# ps = ISRIStemmer() 

datasetsCount = {}
datasetsAll = {}
datasets = os.listdir('datasets')

for file in datasets:
    datasetsAll[file.replace('.json', '')] = 0
    datasetsCount[file.replace('.json', '')] = []

stops = ', . ? ! : ; " ’'.split(' ')
specials = '‘ ~ @ # $ % ^ & * - = + > < [ ] { } / \\ |'.split(' ')
alphabet = 'а б в г д е ё ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я z y x w v u t s r q p o n m l k j i h g f e d c b a'.split(' ')

def alpha_count(frequencies_words, symbols):
    frequency_dict = {}

    for a in symbols:
        frequency_dict[a] = 0

    for word in frequencies_words:
        if word[0].lower() in symbols:
            frequency_dict[word[0].lower()] = word[1]

    nList = []
    for key in frequency_dict:
        nList.append(frequency_dict[key])

    return nList
 

def function(text, m, sentences):
    res = [] #np.array([]) 

    nwords = []
    words = []

    lemWords = m.lemmatize(text)
    # sentences = [lemWords]

    for sentence in sentences:
        for token in sentence:
            nwords.append(token)
            if (token not in func_words):
                words.append(token)

    for sentence in sentences:
        for token in sentence.tokens:
            nwords.append(token.text)
            if (token.text not in func_words):
                words.append(token.text)

    res.append(len(text))
    res.append(emoji.emoji_count(text))
    #print('Feature 1: ' + str())

    fdist = nltk.FreqDist(ch.lower() for ch in text if ch.isalpha())
    res.extend(alpha_count(fdist.most_common(), alphabet))
    # #print('Feature 2: ' + str(fdist.most_common()))

    fdist = nltk.FreqDist(ch for ch in text if ch.isalpha() and ch.isupper())
    res.extend(alpha_count(fdist.most_common(), alphabet))
    #print('Feature 3: ' + str(fdist.most_common()))

    fdist = nltk.FreqDist(ch for ch in text if ch.isalpha() and ch.islower())
    res.extend(alpha_count(fdist.most_common(), alphabet))
    #print('Feature 4-29: ' + str(fdist.most_common()))

    fdist = nltk.FreqDist(ch for ch in text if ch in specials)
    res.extend(alpha_count(fdist.most_common(), specials))
    #print('Feature 30-50: ' + str(fdist.most_common()))

    fdist = nltk.FreqDist(ch for ch in text if ch in stops)
    res.extend(alpha_count(fdist.most_common(), stops))
    #print('Feature 51-58: ' + str(fdist.most_common()))

    fdist = nltk.FreqDist(word for word in nwords if word in func_words)  # уровень слов
    res.extend(alpha_count(fdist.most_common(), func_words))
    # print('Feature 59-208: ' + str(fdist.most_common()))

    res.append(len(sentences))
    # print('Feature 209: ' + str(len(sentences)))

    normWords = set()
    
                                   # уровень слов
    for word in lemWords:
        if word[0].isalpha():  
            normWords.add(word)
    res.append(len(normWords))
    
    print('Feature 210: ' + str(len(normWords)))

    shortWordsCount = 0 
    for word in words:                                                    # уровень слов
        if word[0].isalpha() and len(word) < 4:
            shortWordsCount += 1
    res.append(shortWordsCount)
    #print('Feature 211: ' + str(shortWordsCount))

    count = 0
    length = 0
    for word in words:                                                    # уровень слов
        if word[0].isalpha():
            count += 1
            length += len(word)

    if (count == 0):
        res.append(0)
    else:
        res.append(int(length / count))
    #print('Feature 212: ' + str(int(length / count)))

    length = 0                                                            # уровень слов
    for sent in sentences:
        length += len(sent.text) # 
    if (len(sentences) == 0):
        res.append(0)
    else:
        res.append(int(length / len(sentences)))
    #print('Feature 213: ' + str(int(length / len(sentences))))

    length = 0
    for sent in sentences:                                                # уровень слов
        length += len(word_tokenize(sent.text)) # 
    if (len(sentences) == 0):
        res.append(0)
    else:
        res.append(int(length / len(sentences)))
    # print('Feature 214: ' + str(int(length / len(sentences))))

    shortWordsCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for text in words:                                                    # уровень слов
        if not text[0].isalpha():
            continue
        if len(text) > 12:
            shortWordsCount[12] += 1
        else:
            shortWordsCount[len(text) - 1] += 1

    res.extend(shortWordsCount)
    return res

# m = Mystem()
# nlp = [
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL'),
#     stanza.Pipeline('ru', logging_level='FATAL')
# ]



def calc(file, index):
    with open('datasets/' + file, 'r', encoding='utf-8') as f:
        distros_dict = json.load(f)
        f.close()

    with open('results/' + file, 'a+', encoding='utf-8') as f:
        try:
            results_dict = json.load(f)
        except:
            results_dict = []
        f.close()

    datasetsAll[file.replace('.json', '')] = len(distros_dict)
    datasetsCount[file.replace('.json', '')].extend(results_dict)

    m = Mystem()
    # nlp = stanza.Pipeline('ru', logging_level='FATAL')

    i = 0
    for distro in distros_dict:
        i += 1
        if i <= len(results_dict):
            continue
        
        sentences = 1 #nlp[index % 8](distro).sentences

        tweet = function(distro, m, sentences) 
        tweet.append(index)
        datasetsCount[file.replace('.json', '')].append(tweet)

def status():
    while True:
        for key in datasetsCount:
            if len(datasetsCount[key]) == datasetsAll[key]:
                print('\033[0;37;40m' + key + ': ' + str(len(datasetsCount[key])) + '/' + str(datasetsAll[key]))
                continue

            print('\033[0;37;40m' + key + ': \033[1;32;40m' + str(len(datasetsCount[key])) + '\033[1;37;40m/' + str(datasetsAll[key]))

            with open('results/' + key + '.json', 'w', encoding='utf-8') as f:
                json.dump(datasetsCount[key], f)
                f.close()

        for key in range(len(datasetsCount)):
            sys.stdout.write("\033[A\r")

        time.sleep(3)

index = 0
threads = list()
for file in datasets:
    x = threading.Thread(target=calc, args=(file, index))
    threads.append(x)
    x.start()
    index += 1

x = threading.Thread(target=status, args=())
x.start()

for index, thread in enumerate(threads):
    thread.join()

