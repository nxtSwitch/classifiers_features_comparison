from scipy.stats import spearmanr, pearsonr
import numpy as np
import pandas as pd
import json

with open('results/all.json') as f:
    templates = json.load(f)

df = pd.DataFrame(templates)

t = df[df.shape[1]-1]
t_ind = df.shape[1]-2
df.drop(df.shape[1]-1, axis=1, inplace = True)
df[df.shape[1]+1] = t
t_ind = df.shape[1]

ar = (((df.std()==0).to_numpy()))
null = []

for i in range(len(ar)):
    if ar[i]:
        null.append(i)
    
df.drop(null, axis=1, inplace = True)

names = []
for i in range(df.shape[1]):    
    names.append(i)

df.columns = names
print(df.shape)
#df = shuffle(df)


corr, p_value = spearmanr(df.values, df.values)
a = int(corr[-1][:].shape[0]/2)

s_corr = np.round(corr[-1][:a], 4)
print()
s_pval = np.round(p_value[-1][:a], 4)
a = []
for i in range(len(s_pval)):
    if s_pval[i] >0.05:
        a.append(i)
print(a)
print(len(a))
#print(p_value)

p_corr = []
p_pval = []
for i in range(df.shape[1]):
    corr, p_value = pearsonr(df[i].values, t.values)
    p_corr.append(np.round(corr, 4))
    if p_value >0.05:
        p_pval.append(i)

print(p_corr)
print()
print(p_pval)
print()
print(a)