import json
from math import sqrt
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from scipy.spatial.distance import cosine
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances

with open('results/all.json') as f:
    templates = json.load(f)


df = pd.DataFrame(templates)
y = df[df.shape[1]-1].to_numpy()
ar = (((df.std()==0).to_numpy()))
#print(df.shape)

null = [df.shape[1]-1]
for i in range(len(ar)):
    if ar[i]:
        null.append(i)

df.drop(null, axis=1, inplace = True)
def normalize(x):
    return (x - x.mean())/x.std()
df = df.apply(normalize, axis=0)
templates = df.to_numpy()
#print(templates[0])

# w = pd.read_csv('text.csv')
# w0 = w[w.index==w.shape[0]-1].to_numpy()[0][0]
# w.drop(w.shape[0]-1, inplace = True)
# w = w.to_numpy().reshape(1, -1)

def my_cosine_sim_row(x, y, w = 1, w0 = 0):
    a = np.abs(np.sum((x*y*w))+w0)
    b = np.abs(np.sum((x*x*w)[0])+w0)
    с = np.abs(np.sum((y*y*w)[0])+w0)
    return (a/(sqrt(b)*(sqrt(b))))


def my_minmax_row(x, y, w = 1, w0 = 0):
    a = (np.sum(np.minimum(x, y))+w0)
    b = (np.sum(np.maximum(x, y))+w0)
    return a/b


cosine_matr = np.zeros([2000, 2000])
for i in range(2000):   
    for j in range(i, 2000):
        cosine_matr[i][j] = my_minmax_row(templates[i], templates[j])
    #print(i)

# cos_mat = cosine_matr
# for x in range(cos_mat.shape[0]):
#     cos_mat[x][x] = 0
# #print((cos_mat))
# df_cos = pd.DataFrame(cos_mat)
# y_pred = df_cos.idxmax().to_numpy()


pos = 0
for i in range(y_pred.shape[0]):
    y_p = y_pred[i]
    if y[y_p] == y[i]:
        pos +=1

print(pos/y_pred.shape[0]*100)
#print(cosine_matr)

#print(my_cosine_sim_row(templates[1], templates[50], w, w0))
#print(cosine_similarity(templates, templates)[1][50])
