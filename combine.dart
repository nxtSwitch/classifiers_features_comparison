import 'dart:io';
import 'dart:convert';

import 'package:path/path.dart' as p;

void main(List<String> arguments) async
{
  // final other = await File('other_features.json').readAsString();
  final otherJson = []; //jsonDecode(other);

  final result = [];
  for (File file in Directory.current.listSync().where((f) => f is! Directory)) 
  {
    if (!file.path.contains('.json')) continue;
    if (file.path.contains('other_features.json')) continue; 
    
    final text = await file.readAsString();
    final json = jsonDecode(text);

    result.addAll(json);
    save([...json, ...otherJson], p.basename(file.path));
  }

  print(result.length);
  // save(result, p.basename('features.json'));
}

void save(List features, String name) async
{
  features.shuffle();

  final checkCount = features[0].length;
  
  final flags = <int, bool>{};
  for (final vector in features) {
    for (int e = 0; e < vector.length; e++) 
    {
      flags[e] ??= false;
      if (vector[e] != 0) flags[e] = true;
    }
  }

  for (final test in features) {
    if (checkCount != test.length) {print('error 1'); return;}
  }

  final result = [];
  for (final vector in features) {
    result.add([]);
    for (int e = 0; e < vector.length; e++) 
    {
      if (flags[e]) result.last.add(vector[e]);
    }
  }

  final resCount = result[0].length;
  for (final vector in result) {
    if (resCount != vector.length) {print('error 2'); return;}
  }

  final out = p.join(Directory.current.path, 'results', name);
  final jsonFile = await File(out).create(recursive: true);

  await jsonFile.writeAsString(
    jsonEncode(result.sublist(0, 25000)), 
    mode: FileMode.writeOnly
  );
}
