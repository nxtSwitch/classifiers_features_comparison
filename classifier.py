import numpy as np
import pandas as pd
import math

from sklearn.metrics import accuracy_score, log_loss
from sklearn.preprocessing import OneHotEncoder

from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import decomposition, ensemble
from sklearn.utils import shuffle
import warnings
import json
import os, sys

warnings.filterwarnings('ignore')


def train_model(classifier, feature_vector_train, label, feature_vector_valid, y_test):
    classifier.fit(feature_vector_train, label)
    predictions = classifier.predict(feature_vector_valid)

    return metrics.accuracy_score(predictions, y_test) * 100

def normalize(x):
    return x /x.max()

def calc(templates):
    df = pd.DataFrame(templates)

    t = df[df.shape[1]-1]
    df.drop(df.shape[1]-1, axis=1, inplace = True)
    df[df.shape[1]+1] = t

    ar = (((np.round(df.mean(), 16)==0).to_numpy()))
    null = []

    for i in range(len(ar)):
        if ar[i]:
            null.append(i)
        
    df.drop(null, axis=1, inplace = True)

    names = []
    for i in range(df.shape[1]):    
        names.append(i)

    df.columns = names

    df = shuffle(df)

    fold_size = int(4*df.shape[0]/5)
    TRAIN = df[:fold_size]
    TEST = df[fold_size:]

    y_train = TRAIN[TRAIN.shape[1]-1].to_numpy().reshape(-1, 1)
    y_test = TEST[TEST.shape[1]-1].to_numpy().reshape(-1, 1)

    x_train = TRAIN.drop((TRAIN.shape[1]-1), axis=1).to_numpy().reshape(TRAIN.shape[0], -1)
    x_test = TEST.drop((TEST.shape[1]-1), axis=1).to_numpy().reshape(TEST.shape[0], -1)

    accuracy = train_model(naive_bayes.MultinomialNB(), x_train, y_train, x_test, y_test)
    print ("Naive Bayes: ", accuracy)

    accuracy = train_model(linear_model.LogisticRegression(), x_train, y_train, x_test, y_test)
    print ("Linear Classifier: ", accuracy)

    accuracy = train_model(ensemble.RandomForestClassifier(), x_train, y_train, x_test, y_test)
    print ("Random Forest: ", accuracy)

    accuracy = train_model(svm.SVC(),  x_train, y_train, x_test, y_test)
    print ("Support Vector Machine: ", accuracy)


#################################################

datasets = os.listdir('features')

for file in datasets:
    with open('features/' + file) as f:
        template = json.load(f)

        print('===========================')
        print(file)
        calc(template)
