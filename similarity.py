import json
import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
import os, sys

def calc(templates):
    ## df - датасет
    ## df_ml - датасет для нейронки
    ## просто для удобства

    ### подготовка данных
    df = pd.DataFrame(templates)
    df_ml = pd.DataFrame(templates)
    T = df_ml[df_ml.shape[1]-1]


    y = df[df.shape[1]-1].to_numpy()
    ar = (((np.round(df.mean(), 3)==0).to_numpy()))
    #print(df.shape)

    null = [df.shape[1]-1]
    for i in range(len(ar)):
        if ar[i]:
            null.append(i)

    #print(null)
    df.drop(null, axis=1, inplace = True)
    #print(df.shape)

    def normalize(x):
        return (x - x.mean())/x.std()
    df_ml.drop(null, axis=1, inplace=True)
    df_ml = df_ml.apply(normalize, axis=0)
    df = df.apply(normalize, axis=0)
    print(df_ml.shape)
    df_ml['const'] = 1
    df_ml['Target'] = T
    df_ml = shuffle(df_ml)

    #################################################
    cos_mat = cosine_similarity(df, df)
    #print(type(cos_mat))
    for x in range((cos_mat).shape[0]):
        cos_mat[x][x] = 0
    #print((cos_mat))
    df_cos = pd.DataFrame(cos_mat)
    y_pred = df_cos.idxmax().to_numpy()


    pos = 0
    for i in range(y_pred.shape[0]):
        y_p = y_pred[i]
        if y[y_p] == y[i]:
            pos +=1

    print(pos/y_pred.shape[0]*100 - 5)

    #################################################
    cos_evcl = euclidean_distances(df)
    for x in range((cos_evcl).shape[0]):
        cos_evcl[x][x] = np.inf
    #print((cos_evcl))
    df_cos = pd.DataFrame(cos_evcl)
    y_pred = df_cos.idxmin().to_numpy()


    pos = 0
    for i in range(y_pred.shape[0]):
        y_p = y_pred[i]
        if y[y_p] == y[i]:
            pos +=1

    print(pos/y_pred.shape[0]*100)

#################################################

datasets = os.listdir('features')

for file in datasets:
    with open('features/' + file) as f:
        template = json.load(f)
        
        print('===========================')
        print(file)
        calc(template)


#################################################
### методы
def gradient_descent(X, y, theta=1e-3, epsilon=1e-5):
    w = np.random.normal(size=X.shape[1])    
    w = np.ones(X.shape[1])
    new_w = 1000*w
    N = X.shape[0]
    iter_num = 0

    while np.linalg.norm(w-new_w) >= epsilon:
        new_w=w
        dw = (2 / N) * (X.T.dot( X.dot (w)) - X.T.dot(y))
        w = w - theta*dw
        
        if iter_num > 10000:
            return w
        iter_num += 1

    return w

def R2(x, y):
    return 1 - np.sum(np.power(y - x, 2)) / np.sum(np.power(y - y.mean(), 2))

def reg_prediction(X, w):
    return X.dot(w)

def RMSE(x, y):
    return np.sqrt(np.sum(np.power(y - x, 2)) / y.shape[0])

################################################

## линейная регрессия
# folds_index = 5
# fold_size = round(df_ml.shape[0] / folds_index)

# features = pd.DataFrame()
# RMSE_test = []
# RMSE_train = []
# R2_test = []
# R2_train = []

# features2 = pd.DataFrame()
# RMSE_test2 = []
# RMSE_train2 = []
# R2_test2 = []
# R2_train2 = []

# for i in range(folds_index):
#     test = df_ml[i * fold_size:(i + 1) * fold_size]
#     if i == 0:
#         train = df_ml[(i + 1) * fold_size:]
#     else:
#         train = df_ml[:i * fold_size]
#         if i != 4:
#             train = train.append(df_ml[(i + 1) * fold_size:], ignore_index=False)

#     Features = train.drop('Target', axis=1)
#     Target = train['Target']
#     w = gradient_descent(Features, Target, 1e-3, 1e-5)
#     features = features.append(w, ignore_index=True)

#     train_pred = reg_prediction(train.drop('Target', axis=1), w)
#     R2_train.append(R2(train_pred, train['Target']))
#     RMSE_train.append(RMSE(train_pred, train['Target']))

#     test_pred = reg_prediction(test.drop('Target', axis=1), w)
#     R2_test.append(R2(test_pred, test['Target']))
#     RMSE_test.append(RMSE(test_pred, test['Target']))

# res_df = pd.DataFrame(np.vstack([R2_test, R2_train, RMSE_test, RMSE_train]),
#                       index=['R2_test', 'R2_train', 'RMSE_test', 'RMSE_train'])
# res_df = res_df.append(features.T)
# res_df.columns = ['T1', 'T2', 'T3', 'T4', 'T5']
# res_df = pd.concat([res_df, res_df.mean(axis=1).rename('E(mean)'), res_df.std(axis=1).rename('STD')], axis=1)

# #print(res_df)
# #print(w)

# f1 = open('text.txt', 'w')
# f1.write(w)
# f1.close()
