import 'emojis.dart';
import 'stop_words.dart';
import 'dart_mystem.dart';

final stops = ',.?!:;"’'.split('');
final specials = '‘~@#\$%^&*-=+><[]{}/\\|'.split('');

final eng = 'zyxwvutsrqponmlkjihgfedcba';
final rus = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

List<String> split_text(String text)
{
  return text.runes.map((rune) => String.fromCharCode(rune)).toList();
}

List<int> frequency(List source, List items)
{
  final frequency = <String, int>{};

  for (final item in items) {
    frequency[item] = 0;
  }

  for (String item in source) {
    item = item.toLowerCase(); 
    if (items.contains(item)) frequency[item]++;
  }

  return frequency.values.toList();
}

List<List<String>> sentences(List<String> words) 
{
  final result = <List<String>>[]; 

  result.add([]);
  for (String word in words) {
    if (word == '\\s') {
      result.add([]);
      continue;
    }

    result.last.add(word);
  }

  return result;
}

List specials_features(String text) 
{
  final result = <int>[]; 
  final answerExp = RegExp(r"([@][^\s\@]*)");
  final hashtagExp = RegExp(r"([#][^\s#]*)");

  final answers = answerExp.allMatches(text);
  final hashtags = hashtagExp.allMatches(text);
  final smiles = frequency(split_text(text), emojis);

  result.addAll(smiles);
  result.add(smiles.reduce((total, count) => total + count));

  result.add(answers.length);
  result.add(hashtags.length);

  return result;
}

List symbols_features(String text)
{
  final result = <int>[]; 

  final rusSplit = rus.split('');
  final engSplit = eng.split('');
  final splitText = split_text(text);

  // Feature 1
  result.add(text.length);

  // Feature 2
  result.addAll(frequency(split_text(text.toLowerCase()), rusSplit));
  result.addAll(frequency(split_text(text.toLowerCase()), engSplit));

  // Feature 3
  result.addAll(frequency(splitText, rusSplit));
  result.addAll(frequency(splitText, engSplit));

  // Feature 4
  result.addAll(frequency(splitText, rus.toUpperCase().split('')));
  result.addAll(frequency(splitText, eng.toUpperCase().split('')));

  // Feature 5
  result.addAll(frequency(splitText, specials));

  // Feature 6
  result.addAll(frequency(splitText, stops));

  return result;
}


List words_features(List words)
{
  final result = <int>[]; 

  final s = sentences(words);
  final w = words.where((word) => !stopwords.contains(word) && word != '\\s');

  if (w.isEmpty) return result;
  
  // Feature 7
  result.addAll(frequency(words, stopwords));

  // Feature 8
  result.add(s.length);

  // Feature 9
  final norm = <String>{};
  for (String word in w) { norm.add(word); }
  result.add(norm.length);

  // Feature 10
  result.add(w.fold(0, (count, word) => word.length < 4 ? count + 1 : count));

  // Feature 11
  int len = w.fold(0, (len, word) => len + word.length);
  result.add(len ~/ w.length);

  // Feature 12
  int slen = s.fold(0, (len, s) => len + s.fold(0, (l, w) => l + w.length));
  result.add(slen ~/ s.length);

  // Feature 13
  int wslen = s.fold(0, (len, s) => len + s.length);
  result.add(wslen ~/ s.length);

  // Feature 14
  final shorts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  for (String word in w) { 
    if (word.length > 12) shorts[12]++;
    if (word.length <= 12) shorts[word.length - 1]++;
  }

  result.addAll(shorts);

  return result;
}

Future<List> vector(String text, MyStem mystem) async
{
  final words = await mystem.lemmatize(text);

  if (words.isEmpty || words.length < 3) return [];

  final wordFeatures = words_features(words);
  if (wordFeatures.isEmpty) return [];

  final symFeatures = symbols_features(text);
  if (symFeatures.isEmpty) return [];

  final sFeatures = specials_features(text);
  if (sFeatures.isEmpty) return [];

  return [...symFeatures, ...wordFeatures, ...sFeatures]; 
}
