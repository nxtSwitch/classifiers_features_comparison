import 'dart:io';
import 'dart:async';
import 'dart:convert';

class MyStem
{
  Completer _lock;
  Process _process;
  Stream<String> _broadcast;

  final _initCompleter = Completer();
  final _arguments = <String>['-c', '--format', 'json', '-e', 'utf-8'];

  MyStem({String workingDirectory})
  {
    _arguments.addAll(['-l', '-s']);

    Process.start('mystem', _arguments, workingDirectory: workingDirectory)
      .then((process) {
        _process = process;
        _broadcast = process.stdout.transform(utf8.decoder).asBroadcastStream();
        _initCompleter.complete();
      }, 
      onError: (_) => _initCompleter.completeError('TODO'));
  }

  Future<dynamic> analyze(String text) async
  {
    await _initCompleter.future;

    final lock = _lock;
    _lock = Completer.sync();

    if (lock != null) await lock.future;

    try {
      _process.stdin.writeln(text.replaceAll('\n', ''));
      final result = await _broadcast.first;
      return json.decode(result);
    }
    finally {
      _lock.complete();
    }
  }

  Future<List<String>> lemmatize(String text) async
  {
    List<String> result = [];
    List<dynamic> data = await analyze(text);
    
    for (Map entry in data) {
      if (entry.containsKey('analysis')) {
        List analysis = entry['analysis'];

        if (analysis.isNotEmpty) {
          result.add(analysis[0]['lex']);
          continue;
        }
      }

      final text = (entry['text'] as String).trim();
      if (text.isNotEmpty) result.add(text); //  && text == '\\s'
    }

    return result;
  }

  void close()
  {
    _process.kill();
  }
}

