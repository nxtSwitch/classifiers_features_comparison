import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:isolate_supervisor/isolate_supervisor.dart';

import 'bin/analyze.dart';
import 'bin/dart_mystem.dart';

void main(List<String> arguments) async
{
  final dir = Directory('datasets');
  final supervisor = IsolateSupervisor();

  var list = dir.list(recursive: true).where((item) => item is Directory);

  int id = 0;
  final results = <Future>[];
  await for(final dir in list) {
    results.add(supervisor.compute(streamEntryPoint, [dir.path, id++]));
  }

  final stopwatch = Stopwatch()..start();
  await Future.wait(results);
  stopwatch.stop();

  print(stopwatch.elapsedMilliseconds);
  await supervisor.dispose();
}

Stream<num> streamEntryPoint(IsolateContext context) async*
{
  int id = context.arguments.nearest();
  String path = context.arguments.nearest();
  String category = p.basename(path);
  
  final dir = Directory(path);
  if (!dir.existsSync()) return;

  final mystem = MyStem();
  final features = [];

  final files = Directory(path).list().where((file) => !file.path.contains('features'));

  int i = 0;
  await for(File file in files) {
    final text = await file.readAsString();
    final json = jsonDecode(text);

    for (final tweet in json) {
      final v = await vector(tweet, mystem);
      if (v.isNotEmpty) features.add([...v, id * 100 + i]);
    }
    
    print('$category: ' + p.basename(file.path));
    i++;
  }

  final outPath = p.join(path, './../../results/', '${category}_features.json');
  final jsonFile = await File(outPath).create(recursive: true);
  await jsonFile.writeAsString(jsonEncode(features), mode: FileMode.writeOnly);

  mystem.close();
}
