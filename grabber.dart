import 'dart:io';
import 'dart:convert';

import 'package:path/path.dart' as p;
import 'package:twitter_api/twitter_api.dart';

final oauth = {
  'token': '',
  'tokenSecret': '',
  'consumerKey': '',
  'consumerSecret': ''
};

void main(List<String> arguments) async
{
  final accountsFile = File('accounts.txt');
  if (!accountsFile.existsSync()) {
    print('accounts file error');
    return;
  };

  final accounts = (await accountsFile.readAsString()).split('\n');

  for (final name in accounts) {
    if (name.isEmpty) continue;

    if (await grab(name.trim())) {
      await Future.delayed(Duration(milliseconds: 5000));
    }
  }
}

Future<bool> grab(String accountName) async
{
  final outPath = p.join('twitter', '$accountName.json');
  if (File(outPath).existsSync()) return false;

  int page = 1;
  Set tweets = {};

  print('$accountName');
  while (true) {
    List timeline = await getTimeline(accountName, page);
    if (timeline.isEmpty) break;

    for (final tweet in timeline) 
    {
      final text = tweet['full_text'];
      if (text.indexOf('RT @') == 0) continue;

      final tweetText = text.replaceAll(RegExp(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)'), '').trim();
      if (tweetText.length != 0) tweets.add(tweetText);
    }

    print('    page: $page, tweets: ${tweets.length}');
    final jsonFile = await File(outPath).create(recursive: true);
    await jsonFile.writeAsString(jsonEncode(tweets.toList()));

    page++;
    await Future.delayed(Duration(milliseconds: 500));
  }

  return true;
}

dynamic getTimeline(String screenName, int page) async
{
  final request = await twitterRequest(
    'GET', 
    'statuses/user_timeline.json', 
    options: {
      'screen_name': '$screenName',
      'count': '200',
      'trim_user': 'true',
      'page': '$page',
      'tweet_mode': 'extended',
      'include_entities': '0'
    },
  );

  return jsonDecode(request.body);
}

twitterRequest(String method, String url, {Map<String, String> options}) async 
{
  if(options == null) options = {};

  final signature = TwitterApi.generateSignature(method, url, options, oauth);
  final timestamp = (new DateTime.now().millisecondsSinceEpoch/1000).floor().toString();

  return http.get(
    Uri.https("api.twitter.com", "/1.1/" + url, options), 
    headers: {
      "Content-Type": "application/json",
      "Authorization": 
        'oauth_consumer_key: ${oauth['consumerKey']}, ' +
        'oauth_signature: ${signature}, ' + 
        'oauth_timestamp: ${timestamp}, ' +
        'oauth_token: ${oauth['token']}, ' +
        'oauth_version: 1.0'
    }
  );    
}
